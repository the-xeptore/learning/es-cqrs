#[derive(Debug)]
pub enum CommandKind {
    CreateAccount(CreateAccount),
    DeleteAccount(DeleteAccount),
    ActivateAccount(ActivateAccount),
    DeactivateAccount(DeactivateAccount),
    PerformDeposit(PerformDeposit),
    PerformWithdrawal(PerformWithdrawal),
}

#[derive(Debug)]
pub struct CreateAccount {
    pub account_id: &'static str,
}

#[derive(Debug)]
pub struct DeleteAccount {
    pub account_id: &'static str,
}

#[derive(Debug)]
pub struct ActivateAccount {
    pub account_id: &'static str,
}

#[derive(Debug)]
pub struct DeactivateAccount {
    pub account_id: &'static str,
}

#[derive(Debug)]
pub struct PerformDeposit {
    pub account_id: &'static str,
    pub amount: usize,
}

#[derive(Debug)]
pub struct PerformWithdrawal {
    pub account_id: &'static str,
    pub amount: usize,
}
