use super::{command_handler, commands};

pub struct InMemoryCommandBus {
    handler: command_handler::CommandHandler,
}

impl InMemoryCommandBus {
    pub fn new(handler: command_handler::CommandHandler) -> Self {
        InMemoryCommandBus { handler: handler }
    }

    pub fn handle(&self, cmd: &commands::CommandKind) -> () {
        self.handler.handle(&cmd);
    }
}
