use super::commands;

pub struct CommandHandler();

impl CommandHandler {
    pub fn handle(&self, cmd: &commands::CommandKind) -> () {
        match cmd {
            commands::CommandKind::CreateAccount(c) => self.handle_create_account(c),
            commands::CommandKind::DeactivateAccount(c) => self.handle_deactivate_account(c),
            commands::CommandKind::ActivateAccount(c) => self.handle_activate_account(c),
            commands::CommandKind::DeleteAccount(c) => self.handle_delete_account(c),
            commands::CommandKind::PerformDeposit(c) => self.handle_perform_deposit(c),
            commands::CommandKind::PerformWithdrawal(c) => self.handle_perform_withdrawal(c),
        }
    }

    fn handle_create_account(&self, command: &commands::CreateAccount) -> () {
        println!("handling 'CreateAccount' command: {:#?}", command);
    }

    fn handle_delete_account(&self, command: &commands::DeleteAccount) -> () {
        println!("handling 'DeleteAccount' command: {:#?}", command);
    }

    fn handle_activate_account(&self, command: &commands::ActivateAccount) -> () {
        println!("Handling 'ActivateAccount' command: {:#?}", command);
    }

    fn handle_deactivate_account(&self, command: &commands::DeactivateAccount) -> () {
        println!("Handling 'DeactivateAccount' command: {:#?}", command);
    }

    fn handle_perform_deposit(&self, command: &commands::PerformDeposit) -> () {
        println!("Handling 'PerformDeposit' command: {:#?}", command);
    }

    fn handle_perform_withdrawal(&self, command: &commands::PerformWithdrawal) -> () {
        println!("Handling 'PerformWithdrawal' command: {:#?}", command);
    }
}
