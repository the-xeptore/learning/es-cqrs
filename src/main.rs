use es::{command_bus, command_handler, commands};

fn main() {
    // wiring
    let cmd_handler = command_handler::CommandHandler{};
    let bus = command_bus::InMemoryCommandBus::new(cmd_handler);

    let cmd = commands::CommandKind::CreateAccount(
        commands::CreateAccount{ account_id: "6ee7cebe-91b4-433a-9c44-48ffce9dc32e" },
    );

    bus.handle(&cmd);
}
